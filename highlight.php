<?php
/**
 * Highlight Plugin
 *
 * @author Julien Gargot <julien@g-u-i.me>
 * @version 1.0.0
 */
function highlight($kirbytext, $search) {

  //- Pattern construction
  // start the pattern
  $pattern = '/(';
  // Add each word of “search” to the pattern as OR.
  preg_match_all("/\w+/", $search, $words);
  $first = $words[0][0];
  foreach ($words[0] as $word)
  {
    if ( strlen($word) > 2)
    {
      if ($word != $first)
      {
        $pattern .= '|';
      }
      $pattern .= $word;
    }
  }
  // end the pattern
  $pattern .= ')/i';

  //- Replacement construction
  $replacement = '<span class="' . c::get('highlight.class', 'highlight') . '">$1</span>';

  //- Return the result
  return preg_replace($pattern, $replacement, $kirbytext);
};
